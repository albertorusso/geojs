<?php
//define project and license
/*
* GeoJs: project for recorder the geo point using the API of GOOGLE MAPS v3
*
* Copyright (C) 2013  Alberto Russo <info@albertorusso.it>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as
* published by the Free Software Foundation, either version 3 of the
* License, or (at your option) any later version.
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

if(!@isset($_SERVER['HTTP_X_REQUESTED_WITH'])) die('Error: unauthorized');

if(!@isset($_POST['action'])) die('Error request');

if($_POST['action']=="save")
{
    $latitude = $_POST['lat'];
    $longitude = $_POST['lon'];
    $desc = $_POST['desc'];
    
    saveCoords($latitude, $longitude, $desc);
    
}
elseif ($_POST['action']=="read")
{
    $date = $_POST['date'];
    
    readCoords($date);
}
elseif ($_POST['action']=="date")
{
    getDate();
}


function openConnection()
{
    require_once 'config.php';
   // echo "$dsn:host=$host;dbname=$dbname";
    try
    {
        $db = new PDO("$dsn:host=$host;dbname=$dbname", $user, $pass);
    }
    catch (PDOException $e)
    {
        print "Error!: " . $e->getMessage() . "<br/>";
    }   
    return $db;
}

function saveCoords($latitude, $longitude, $desc)
{
    
    $db=openConnection();
    $date = date("Y-m-d");
    $time = date("H:i:s");
    try
    { 
        $query = $db->prepare("INSERT INTO `coords`(`Lat`, `Lon`, `Date`, `Time`, `Desc`) VALUES( :lat, :lon, :date , :time , :desc )");
        
        $query->bindParam(':lat', $latitude);
        $query->bindParam(':lon', $longitude);
        $query->bindParam(':date', $date);
        $query->bindParam(':time', $time);
        $query->bindParam(':desc', $db->quote($desc));
        
        $bool = $query->execute();
        
        closeConnection($db);
        if ($bool) echo "Inserimento avvenuto con successo.";
        else print_r($query->errorInfo());
        
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();        
    }
    
}


function readCoords($date)
{
    $db=openConnection();
    try
    {
        $query = $db->prepare("SELECT `Lat`, `Lon`, `Time`, `Desc` FROM `coords` WHERE `Date`='$date' ORDER BY `Time`");
    
        $query->execute();
        
        $res = $query-> fetchAll(PDO::FETCH_ASSOC);
    
        closeConnection($db);
    
        echo json_encode($res);
    
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    
        return NULL;
    }
}

function getDate()
{
    $db=openConnection();
    try
    {
        $query = $db->prepare("SELECT `Date` FROM `coords` GROUP BY `Date`");
        $query->execute();
        
        $res = $query-> fetchAll(PDO::FETCH_ASSOC);
    
        closeConnection($db);
    
        echo json_encode($res);
    
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    
        return NULL;
    }
}

function closeConnection($db)
{
    $db = null;
}



//End file:  geoSave.php
