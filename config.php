<?php
/*
 * GeoJs: project for recorder the geo point using the API of GOOGLE MAPS v3
*
* Copyright (C) 2013  Alberto Russo <info@albertorusso.it>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as
* published by the Free Software Foundation, either version 3 of the
* License, or (at your option) any later version.
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/


//Data Source Name: cubrid , dblib , firebird , ibm , informix , mysql , sqlsrv , oci , odbc , pgsql , sqlite , 4D ;
$dsn='mysql';

//Hostname database
$host='hostname';

//Name database
$dbname='database_name';

//Username database
$user='database_username';

//Password database
$pass='database_password';

//End file:  config.php
