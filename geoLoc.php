<?php

//define project and license
/*
 * GeoJs: project for recorder the geo point using the API of GOOGLE MAPS v3
 * 
 * Copyright (C) 2013  Alberto Russo <info@albertorusso.it>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


//define info project
/*
 * API key:
 */

?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    	<style type="text/css">
            html { height: 100% }
            body { height: 100%; margin: 0; padding: 0 }
            #map { height: 400px }
            #click{ background-color: #222222; height: 100px; width: 100px;}
    	</style>
    	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="http://code.jquery.com/jquery-migrate-1.1.1.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?&sensor=true">
    	</script>
    	<script type="text/javascript">
    	    $(document).ready(function()
            {
                var lat=0;
                var lon=0;
                $('#data').hide();
                if(navigator.geolocation)//supported
                {
                    navigator.geolocation.getCurrentPosition(latlon);
                }
                else//Not supported
                {
                    error("Not supported");
                }

                //show map
                function latlon(myevent)
                {	
                    lat=myevent.coords.latitude;
                    lon=myevent.coords.longitude;
                   
                    var point = new google.maps.LatLng(lat,lon );
                    // map options          
                    options = 
                    {
                        zoom: 9,
                        center: punto,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    // define box
                    box = document.getElementById("map");
                    // create map
                    map = new google.maps.Map(box, opzioni);
                    //marker
                    marker = new google.maps.Marker(
                    {
                        position: point,
                        map: map,
                        title: "Here"
                    });
                    
                    $('#data').show();
                }

                $('#click').click(function()
                {
                	
                    var desc = $("#desc").val();
                	$.post("geoDB.php", { action: "save", lat: lat, lon: lon, desc: desc }, function(msg) {
                		alert(msg);
                	}).error(function () {
                		alert("error");
					});
                });
            });
        </script>
    </head>
    <body>
        <div id="data" align="center">
            <p>
                <label for="desc"> Info</label><input type="text" name="desc" id="desc">
            </p>
            <p>
                <input id="click" type="button" value="Insert new data">
            </p>
        </div>
	<div id="map"></div>
    </body>
</html>
<?php

//End file:  geoloc.php
