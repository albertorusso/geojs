<?php

//define project and license
/*
 * GeoJs: project for recorder the geo point using the API of GOOGLE MAPS v3
 * 
 * Copyright (C) 2013  Alberto Russo <info@albertorusso.it>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


//define info project
/*
 * API key: 
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    	<style type="text/css">
            html { height: 100% }
            body { height: 100%; margin: 0; padding: 0 }
            #map { height: 600px; width: 600px }
            #click{ background-color: #222222; height: 100px; width: 100px;}
    	</style>
    	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	    <script src="http://code.jquery.com/jquery-migrate-1.1.1.min.js"></script>
	    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true">
    	</script>
    	<script type="text/javascript">
    	    $(document).ready(function()
            {
    	    	var map;
    	    	var markersArray = new Array();
    	    	var infowindow = null;
                google.maps.event.addDomListener(window, 'load', initialize);

                infowindow = new google.maps.InfoWindow({
                	content: "holding..."
                });
                
                //show map
                function initialize()
                {	
                    var center = new google.maps.LatLng(0,0);
                   
                    // set options  
                    options = 
                    {
                        zoom: 10,
                        center: center,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    },
                    // define box
                    box = document.getElementById("map");
                    // creo la mappa
                    map = new google.maps.Map(box, options);

                    
                    
                    
                };

                $.fn.init= function() {
                	$.post("geoDB.php", { action: "date" }, function(text) {
                	    var json = eval(text);
                	    for (var i = 0 ; i < json.length; i++) {
                    	    var date = new Date(json[i].Date);
                    	    if(i!=json.length-1){
                    	        document.getElementById("date").innerHTML += '<OPTION VALUE="'+ json[i].Date + '">' + date.getDate() +"/"+ (date.getMonth()+1) + "/" + date.getFullYear();
                    	    }
                    	    else{
                    	    	document.getElementById("date").innerHTML += '<OPTION SELECTED VALUE="'+ json[i].Date + '">' + date.getDate() +"/"+ (date.getMonth()+1) + "/" + date.getFullYear();
                    	    }
                	    }
                	    $.fn.read();
                	    
                	}).error(function () {
                		alert("error");
    				}, "json");
                };

                
                $.fn.read = function() {
                	$.fn.resetMaker();
                    var date = document.getElementById("date").options[document.getElementById("date").selectedIndex].value;
                	$.post("geoDB.php", { action: "read", date: date }, function(text) {
                		var json = eval(text);
                	    for (var i = 0 ; i < json.length; i++) {
                	    	
                	        var lat = json[i].Lat;
                	        var lon = json[i].Lon;
                	        var time = json[i].Time;
                	        var desc =json[i].Desc;
                	        $.fn.loadMaker(lat, lon, desc, time);
                	    }
                	    $.fn.autoCenter();
                	}).error(function () {
                		alert("error");
    				},"json");
                    
                };

                $.fn.loadMaker = function(lat, lon, desc, time) {
                    var center = new google.maps.LatLng(lat,lon);                    
                    
                    marker = new google.maps.Marker(
                    {
                        position: center,
                        map: map,
                        animation: google.maps.Animation.DROP,
                        title: desc,                        
                        html: $.fn.getInfo(time,desc)
                    });
                    
                    google.maps.event.addListener(marker, 'click', function () {
                    	// where I have added .html to the marker object.
                    	infowindow.setContent(this.html);
                    	infowindow.open(map, this);
                    });

                    markersArray[markersArray.length] = marker;
				};

				$.fn.resetMaker = function()
				{
					for (var i = 0 ; i < markersArray.length; i++) {
						markersArray[i].setMap(null);
            	    }
					
				};

				$.fn.getInfo = function (time,desc) {
					var contentString = '<h2>'+desc.replace(/\'/g, '')+'</h2>';
					contentString += 'at: ' + time;				
					return contentString;
				};

				$.fn.autoCenter = function () {
    				//  Create a new viewpoint bound
    				var bounds = new google.maps.LatLngBounds();
    				//  Go through each...
    				$.each(markersArray, function (index, marker) {
    				    bounds.extend(marker.position);
    				});
    				//  Fit these bounds to the map
    				mappa.fitBounds(bounds);
				};
                
            });
        </script>
    </head>
    <body>
        <select id="date"></select>
        <input type="submit" value="Show" onclick="$.fn.read();">
        <?php 
        
        ?>
        <div id="map"></div>
        <script type="text/javascript">$.fn.init();</script>
    </body>
</html>
<?php

//End file:  geoView.php
